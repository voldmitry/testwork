//
//  ViewController.m
//  testWork
//
//  Created by Дмитрий on 16.11.16.
//  Copyright © 2016 Vol.dmitry. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *grant_typeStr = @"password";
    NSString *usernameStr = @"test";
    NSString *passwordStr = @"1";
    
    NSString *urlSring = @"http://authorization.smiber.com:8000/oauth/token";
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration
                                                          delegate:nil
                                                     delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURL *url = [NSURL URLWithString:urlSring];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = @"POST";
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    NSDictionary *parameters =  @{ @"grant_type": grant_typeStr, @"username": usernameStr, @"password" : passwordStr };
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:kNilOptions error:nil];
    [request setHTTPBody:postData];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSLog(@"Response is: %@", response);
    }];
    
    [postDataTask resume];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
